$(function () {
    var inputText, writeSelect, boxList, innerList;
    boxList = $('.todo_check');
    innerList = boxList.children('ul');
    var areaSort = $('.box_importance > div');
    var sortInp = areaSort.children('input');
    var itemList = innerList.children('li');
    var itemInp = itemList.children('input');
    var itemLab = itemList.children('label');
    var labTitle = itemLab.children('strong').children('em');

    function submitClick() {
        var ListNum = 1;
        $('.btn_submit').click(function () {
            ListNum++;
            var dt = new Date();
            var month = dt.getMonth() + 1;
            var day = dt.getDate();
            var year = dt.getFullYear();
            var nowTime = year + '.' + month + '.' + day;
            inputText = $('.inp_text').val();
            writeSelect = $('.opt_importance option:selected').text();
            var addStr;
            if(writeSelect =='매우중요'){
                addStr = 'item_very';
            }else if(writeSelect =='중요'){
                addStr = 'item_some';
            }else if(writeSelect =='보통'){
                addStr = 'item_normal'
            }else{
                return false;
            }

            var addHtml = '<li class="item_todo '+addStr+'">' 
                            + '<input id="itemChk' + ListNum + '" type="checkbox" aria-pressed="false">' 
                            + '<label for="itemChk' + ListNum + '" class="lnb_todo">' 
                                + '<span class="screen_out">미완료</span>' 
                                + '<span class="txt_import">중요도 : <em>' + writeSelect + '</em></span>' 
                                + '<span class="txt_day">추가일 : <em>' + nowTime + '</em></span>' 
                                + '<span class="txt_todo">' + inputText + '</span>' 
                            + '</label>' 
                            + '<div class="box_drag">' 
                                + '<a href="" class="btn_drag">드래그&amp;드랍</a>' 
                            + '</div>' 
                        + '</li>';
            if (writeSelect == '중요도') {
                alert('중요도를 선택하세요');
                Event.preventDefault;
            } else if (inputText == "") {
                alert('할 일을 입력하세요');
                Event.preventDefault;
            } else {
                innerList.append(addHtml);
                Event.preventDefault;
            }
            
            console.log(ListNum);
            console.log(nowTime);
            console.log(inputText);
            console.log(writeSelect);
            console.log(addHtml);
            
        });
        
    }
    $('.box_importance .inner_importance .lnb_import').on("change",function(){
         //체크박스 순회할 함수();
         function importantClass(){
             
         }
         function completeClass(){
             
         }
         //체크된 해당 엘리먼트 보여줄 함수();
    });
    function classSort() {
        $('.box_importance .inner_importance input').click(function () {
            /* 모두 vs 나머지 구분하여 두부류중 한가지만 클릭되게 구현 */
            var good = $(this).attr('data-class');
            console.log(good);
            /* 리스트 전체 숨김 */
            itemList.hide();
            /* 해당클레스를 가진 li만 보여지게 구현 */
        });
    }

    function chkTitle() {
        var chkNum = 1;
        itemInp.click(function () {
            chkNum++
            /* 모두 vs 나머지 구분하여 두부류중 한가지만 클릭되게 구현 */
            /* 클릭된 엘리먼트의 data-class 값 받아오기 */
            if (chkNum % 2 == 0) {
                $(this).next('label').children('.screen_out').html('완료');
                $(this).parent('li').removeClass('item_incomplete');
                $(this).parent('li').addClass('item_complete')
            } else {
                $(this).next('label').children('.screen_out').html('미완료');
                $(this).parent('li').removeClass('item_complete');
                $(this).parent('li').addClass('item_incomplete');
            }
            /* 
                data-class 값과 같은 class를 가진 클래스 && list_arar에 자식 input중 활성화 된 클래스를 가진 엘리먼트만 보여지기
            */
            
        });
    }

    submitClick();
    classSort();
    chkTitle();
});